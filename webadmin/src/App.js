import React from 'react'
import { BrowserRouter,Switch} from 'react-router-dom'
import { Route } from 'react-router'
import DashboardLayout from './route/DashboardLayout'

export default function App() {
  return (
    <div>
      <BrowserRouter>
          <Switch>
            <Route path="/" component={DashboardLayout}/>
          </Switch>
      </BrowserRouter>
    </div>
  )
}
