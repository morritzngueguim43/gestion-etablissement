/* eslint-disable no-undef */
import React, { useEffect, useState } from 'react';
import { AppBar, Avatar, Box, Button, ButtonGroup, Drawer, IconButton, Input, List, ListItem, ListItemIcon, ListItemText, Toolbar, Typography } from '@material-ui/core';
import { Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { AddCircleOutlineOutlined, Bookmarks, ChevronLeft, ChevronRight, Class, SubjectOutlined } from '@material-ui/icons'
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { Menu } from '@material-ui/icons';
import Logout from '../pages/logout';
import { styled, useTheme } from '@material-ui/styles';
import { Dashboard } from '@material-ui/icons';


const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    //padding: theme.spacing(1),
    // necessary for content to be below app bar
   // ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  }));
const drawerWidth = 200
const useStyle = makeStyles((theme) => {
    return {
        link: {
            color: 'white',
            textDecoration: 'none',
            hover: {
                color: 'red'
            }
        },
        toolbar: theme.mixins.toolbar,
        root: {
            display: 'flex'
        },
        menu: {
            margin: '10px',
        },
        AppBar: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
        content: {
            marginLeft: drawerWidth,
            background: 'f9f9f9',
            width: '100%'
        },
        drawerPaper: {
            width: drawerWidth
        },
        photo: {
            padding: theme.spacing(0),

        },
        notify: {
            padding: '0px 5px',
            backgroundColor:'red',
            borderRadius:'100%',
            color:'white',
            position:'relative',
            top:'-23px',
            left:'-15px',
            fontSize:'12px'

        },
        image: {
            padding: theme.spacing(1),

        },
        active: {
            background: '#f4f4f4'
        },
        element: {
            flexGrow: 1,
            marginLeft: theme.spacing(1),
        },
        btn: {
            borderRadius:'100%'
        },
        title:{
            borderBottom:'1px solid rgba(0,0,0,0.1)'
        }
    }
})

export default function MenuCompte({ children }) {
    const theme = useTheme()
    const [open, setOpen] = useState(false)
    const [visibleDrawer,setVisibleDrawer] = useState(false)
    const classes = useStyle()
    const history = useHistory()
    const location = useLocation()
    const menuItems = [
        {
            text: 'Dashboard',
            icon: <Dashboard color="primary" />,
            path: '/admin'
        },
        {
            text: 'secretaires',
            icon: <AccountBoxIcon color="primary"  />,
            path: '/admin/secretaires'
        },
        {
            text: 'censeurs',
            icon: < AccountBoxIcon color="primary"  />,
            path: '/admin/censeurs'
        },
        {
            text: 'surveillants',
            icon: < AccountBoxIcon color="primary"  />,
            path: '/compte/surveillants'
        },
        {
            text: 'professeurs',
            icon: < AccountBoxIcon color="primary"  />,
            path: '/admin/Professeurs'
        },
        {
            text: 'classes',
            icon: < AccountBoxIcon color="primary"  />,
            path: '/admin/classes'
        },
        {
            text: 'Elèves',
            icon: < Class color="primary"  />,
            path: '/admin/eleves'
        },
        {
            text: 'Cours',
            icon: < Bookmarks color="primary"  />,
            path: '/admin/cours'
        },
    ]

    const handleOpen = () =>{
        setOpen(!open)
    }

    const hasWindow = typeof window !== 'undefined';

    useEffect(() => {
        if(window.innerWidth > 900 ){
            setVisibleDrawer(true)
        }
        window.addEventListener('resize',function(){
            console.log(window.innerWidth)
            if(window.innerWidth > 900 ){
                setVisibleDrawer(true)
            }else{
                setVisibleDrawer(false)
            }
    })
    return () => window.removeEventListener('resize',function(){
        alert(window.innerWidth)
    })
    },[])
    

    return <Container className={classes.root}>
        <AppBar
            className={visibleDrawer ? classes.AppBar : null}
            position="fixed"
            color='inherit'
        >
            <Toolbar>
                <Button style={ visibleDrawer ? {display:'none'} : {display:'block'}} onClick={ () => handleOpen()}>
                    <Menu style={open ? {display:'none'}: {display:'block'} }/>
                </Button>
                <Typography >
                    <Button className={classes.btn}>
                        <Avatar
                            className={classes.photo}>
                            B
                        </Avatar>
                    </Button>
                </Typography>
                <Typography className={classes.element}>Poste: nom_administrateur </Typography>
                <Box mr={5}>
                    <NotificationsIcon color="primary" style={{fontSize:'35px'}}/>
                    <Box component="span" className={classes.notify}>
                        
                    </Box>
                </Box>
                    <Logout />
            </Toolbar>
        </AppBar>
        <Toolbar />
        <Drawer
            variant={visibleDrawer ? 'permanent' : 'persistent'}
            anchor="left"
            open={open}
            color="secondary"
            className={classes.drawer}
            classes={{ paper: classes.drawerPaper }}
        >
            <DrawerHeader>
          <IconButton style={ visibleDrawer ? {display:'none'} : {display:'block'}} onClick={handleOpen}>
             <ChevronLeft />
          </IconButton>
        </DrawerHeader>
            <Box component="span" p={2} className={classes.title}>
                
                <Typography component="h4" variant="h4" >
                 <img src="kjkjk"/>MyScol
                </Typography>
            </Box>
            <List>
                {
                    menuItems.map((menu) => {
                        return (
                            <ListItem
                                button
                                key={menu.text}
                                onClick={() => {
                                    handleOpen()
                                    history.push(menu.path)
                                }}
                                className={location.pathname == menu.path ? classes.active : null}>
                                <ListItemIcon>{menu.icon}</ListItemIcon>
                                <ListItemText>{menu.text}</ListItemText>
                            </ListItem>
                        )
                    })
                }
            </List>
        </Drawer>
        <div className={ visibleDrawer ? classes.content : null}>
            <div className={classes.toolbar} />
            {children}
        </div>

    </Container>
}
