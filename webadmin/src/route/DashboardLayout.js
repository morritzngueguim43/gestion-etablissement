import React from 'react'
import { Route, Switch } from 'react-router'
import Accueil from '../pages/Accueil'
import Dashboard from '../pages/Dashboard'
import MenuCompte from './MenuCompte'

export default function DashboardLayout() {
    return (
        <div>
            <Switch>
                <MenuCompte>
                    <Route path="/admin" component={Dashboard}/>
                </MenuCompte>
            </Switch>
        </div>
    )
}
