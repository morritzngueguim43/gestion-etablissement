import React from 'react'
import { Grid } from '@material-ui/core'
import DashboardItem from '../components/DashbordItem'
import { Class } from '@material-ui/icons'
import { Bookmarks } from '@material-ui/icons'

export default function Dashboard() {
    return (
        <div>
            <Grid container direction="row" spacing={3}>
                <DashboardItem
                    title="Censeurs"
                    to="admin/censeurs" />
                <DashboardItem
                    title="Secretaires"
                    to="admin/secretaires" />
                <DashboardItem
                    title="Surveillants"
                    
                    to="admin/surveillants" />
                <DashboardItem
                    title="Professeurs"
                    to="admin/professeurs" />
                <DashboardItem
                    title="Elèves"
                    to="admin/eleves" />
                <DashboardItem
                    title="Classes"
                    Icon={ <Class/>}
                    to="admin/classes" />
                <DashboardItem
                    title="Matières"
                    Icon={ <Bookmarks/>}
                    to="admin/matière" />
            </Grid>

        </div>
    )
}
